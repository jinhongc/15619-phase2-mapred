import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Reducer {

    static String key;
    static Map<String, Integer> value = new HashMap<>();
    static String[] splits;

    public static void main(String[] args){
        hdStreaming();
    }

    public static void init() {
	key = splits[0];
	value = new HashMap<>();
    }

    // word-uid date:count
    public static void hdStreaming() {
        try{
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(System.in));
            String input;
            while((input=br.readLine())!=null){
		splits = input.split("\t");
		if(key != null && splits[0].equals(key)) {
		    accumulate();
		} else if(key != null){
		    spew();
		    init();
		    accumulate();
		} else {
		    init();
		    accumulate();
		}
            }
	    spew();
        }catch(IOException io){
            io.printStackTrace();
        }
    }

    public static void spew() {
	System.out.println(key + "\t" + map2str());
    }

    public static String map2str() {
	StringBuilder sb = new StringBuilder();
	for(Map.Entry<String, Integer> pair : value.entrySet()) {
	    sb.append(pair.getKey() + ":" + pair.getValue() + ";");
	}
	return sb.toString();
    }

    public static void accumulate() {
	String[] date_count = splits[1].split(":");
	String date = date_count[0];
	int    count = Integer.parseInt(date_count[1]);
	value.put(date, value.get(date) == null ? 1 : value.get(date) + 1);
    }

}
