
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Filter {
    static JSONParser parser = new JSONParser();

    static String[] stopwords_arr = {
            "a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be","because","been","but","by","can","cannot","could","did","do","does","either","else","ever","every","for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","if","in","into","is","it","its","just","least","let","likely","may","me","might","most","must","my","neither","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she","should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet","you","your"
    };
    static Set<String> stopwords = new HashSet<>(Arrays.asList(stopwords_arr));
    static SimpleDateFormat inft = new SimpleDateFormat ("EEE MMM dd hh:mm:ss zzzzz yyyy");
    static SimpleDateFormat outft = new SimpleDateFormat ("yyyy-MM-dd");
    static String uid, date, text;

    public static void filt(String s) {
        JSONObject o;


        // check malformed
        try {
            o =  (JSONObject) parser.parse(s);
        } catch (ParseException e) {
            // cannot be parsed as JSON object
            // if cannot parse, it's a malformed
            System.out.println("Object malformed.");
            return;
        }

        // id and id_str exist
        if(o.get("id") == null && o.get("id_str") == null) {
            return;
        }

        // 'created_at'"," 'text' and 'entities' must exist
        if(o.get("created_at") == null || o.get("text") == null || o.get("entities") == null) {
            return;
        }

        // check 'lang' field"," must exist and equals to 'en'

        if(o.get("lang") == null || !o.get("lang").equals("en"))
            return;


        uid = (String) ((JSONObject) o.get("user")).get("id_str");

	date = parseDate((String) o.get("created_at")); // TODO, parse date
        text = ((String) o.get("text")).replaceAll("(https?|ftp):\\/\\/[^\\s/$.?#][^\\s]*", "").toLowerCase();
	printWordCount(text);
    }

    private static void printWordCount(String text) {
        text = text.replaceAll("[^a-zA-Z0-9]+", " ");
        String[] words = text.split(" ");
        Map<String, Integer> counter = new HashMap<String, Integer>();
        for(String w : words) {
            counter.put(w, counter.get(w) == null ? 1 : counter.get(w));
        }

        StringBuilder sb = new StringBuilder();

        for(Map.Entry<String, Integer> pair : counter.entrySet()) {
            if(pair.getKey().equals("") || stopwords.contains(pair.getKey())) continue;
	    System.out.println(pair.getKey() + "-" + uid + "\t" + date + ":" + pair.getValue());
        }
    }

    private static String parseDate(String date) {
        // System.out.println(date); //  yyyy.MM.dd 'at' hh:mm:ss a zzz
        // SimpleDateFormat inft = new SimpleDateFormat ("a b d T z Y");
        Date t;
        try {
            t = inft.parse(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return null;
        }
        return outft.format(t);
    }

    public static void main(String[] args){
        hdStreaming();
    }

    public static void hdStreaming() {

        try{
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(System.in));
            String input;
            //While we have input on stdin
            while((input=br.readLine())!=null){
		filt(input);
            }

        }catch(IOException io){
            io.printStackTrace();
        }

    }

}
