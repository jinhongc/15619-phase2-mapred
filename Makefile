all:
	make pack
run:
	java -cp target/mapred-1.0.jar-1-jar-with-dependencies.jar Reducer
	# mvn exec:java -Dexec.mainClass="edu.cmu.jinhongc.mapred.Filter"
pack:
	mvn clean compile assembly:single
deploy-code:
	aws s3 cp target/mapred-1.0.jar-1-jar-with-dependencies.jar s3://cc.ta/test/phase2/code/
